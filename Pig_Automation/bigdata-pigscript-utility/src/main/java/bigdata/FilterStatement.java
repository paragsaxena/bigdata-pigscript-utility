package bigdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FilterStatement {
	private StringBuilder filterStmt = new StringBuilder();

	public Map<String, Set<String>> FilterStatement(Map<String, Set<String>> loadSourceAlias,
			FileOutputStream outputStream) throws IOException {
		Map<String, Set<String>> filterAlias = new HashMap<String, Set<String>>();
		Map<String, Map<String, String>> filterInfo = new HashMap<String, Map<String, String>>();
		filterInfo = GenerateInfo.GenerateFilterInfo();

		/*
		 * for(String es : filterInfo.keySet()) { System.out.println("Outer: "
		 * +es+" Value: "+filterInfo.get(es).toString()); for (String e :
		 * filterInfo.get(es).keySet()) { System.out.println("Outer: "+es+
		 * " Inner: "+e+" Value: "+filterInfo.get(es).get(e).toString()); } }
		 */

		for (String entryset : filterInfo.keySet()) {
			int numberColumns = filterInfo.get(entryset).size();
			String tableName = entryset.substring(entryset.indexOf("_"));
			if (numberColumns > 1)
				filterStmt.append("filter" + tableName + " = FILTER " + entryset + " BY " + "(");
			else
				filterStmt.append("filter" + tableName + " = FILTER " + entryset + " BY ");
			// filterStmt.append(""+UpdateStatement(filterInfo.get(entryset)));
			UpdateStatement(filterInfo.get(entryset));

			for (String newentry : loadSourceAlias.keySet()) {
				String name = newentry.substring(newentry.indexOf("_"));
				filterAlias.put("filter" + name, loadSourceAlias.get(newentry));
			}
		}
		WriteStatement(outputStream);
		return loadSourceAlias;
	}

	private void WriteStatement(OutputStream outputStream) {
		System.out.println(filterStmt.substring(0, filterStmt.length() - 6));
		String temp = filterStmt.substring(0, filterStmt.length() - 6);
		byte[] bytesArray = temp.concat(";").getBytes();
		try {
			outputStream.write(bytesArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void UpdateStatement(Map<String, String> columnCondition) {
		// StringBuilder result = new StringBuilder();
		for (String column : columnCondition.keySet()) {
			String condition = columnCondition.get(column);

			String[] words = condition.split(" ");
			String temp = "";
			for (String word : words) {
				if (!word.equalsIgnoreCase("AND") && !word.equalsIgnoreCase("OR"))
					temp = temp + " " + word;
				else if (word.equalsIgnoreCase("AND")) {
					filterStmt.append(UpdateCondition(column, temp));
					temp = "AND";
				} else if (word.equalsIgnoreCase("OR")) {
					filterStmt.append(UpdateCondition(column, temp));
					temp = "OR";
				}
			}
			filterStmt.append(UpdateCondition(column, temp));
			filterStmt.append(") AND (");
		}
		// return result.toString();
	}

	/*
	 * String returnParsed(String column, String cond){ String condition =
	 * cond.trim(); System.out.println("column: "+column+" Condition: "
	 * +condition); StringBuilder result = new StringBuilder(); if
	 * (condition.contains("!(")) { if
	 * (condition.toUpperCase().contains("AND")||condition.toUpperCase().
	 * contains("OR")) { for (int i=(condition.indexOf("(")+1);
	 * i<condition.length(); i=i+2) { char c = condition.charAt(i);
	 * result.append(" AND "+column+"!="+c+" "); }
	 * result.insert(condition.indexOf("\\s"), result.toString()); }
	 * 
	 * else if (condition.toUpperCase().contains("OR")) { for (int
	 * i=(condition.indexOf("(")+1); i<condition.length(); i=i+2) { char c =
	 * condition.charAt(i); result.append(" AND "+column+"!="+c+" "); } } else {
	 * for (int i=(condition.indexOf("(")+1); i<condition.length(); i=i+2) {
	 * char c = condition.charAt(i); result.append(" "+column+"!="+c+" "); } } }
	 * else if (condition.contains("(")) { if
	 * (condition.toUpperCase().contains("AND")) { for (int
	 * i=(condition.indexOf("(")+1); i<condition.length(); i=i+2) { char c =
	 * condition.charAt(i); result.append(" AND "+column+"=="+c+" "); } } else
	 * if (condition.toUpperCase().contains("OR")) { for (int
	 * i=(condition.indexOf("(")+1); i<condition.length(); i=i+2) { char c =
	 * condition.charAt(i); result.append(" OR "+column+"=="+c+" "); } } else {
	 * for (int i=(condition.indexOf("(")+1); i<condition.length(); i=i+2) { if
	 * (i==(condition.length()-2)) { char c = condition.charAt(i);
	 * result.append(" "+column+"=="+c+" "); } else { char c =
	 * condition.charAt(i); result.append(" "+column+"=="+c+" AND "); } } } }
	 * else { if (condition.toUpperCase().contains("NULL")) result.append(" "
	 * +condition.substring(0,condition.indexOf(" "))+" "+column+" "
	 * +condition.substring(condition.indexOf(" "))); else { result.append(" "
	 * +condition.substring(0,condition.indexOf(" "))+" "+column+" "
	 * +condition.substring(condition.indexOf(" ")+1)); } } return "";
	 * 
	 * return null; }
	 */
	private StringBuilder UpdateCondition(String column, String cond) {
		String condition = cond.trim();
		System.out.println("column: " + column + " Condition: " + condition);
		StringBuilder result = new StringBuilder();
		String notChar = "";
		if (condition.contains("!(")) {
			notChar = "!=";
		} else if (condition.contains("(")) {
			notChar = "==";
		} else {
			if (condition.toUpperCase().contains("NULL")){
				result.append(" " + condition.substring(0, condition.indexOf(" ")) + condition.substring(condition.indexOf(" "))+" ");
			}
			else {
				result.append(" " + condition.substring(0, condition.indexOf(" ")) + " " + column + " "
						+ condition.substring(condition.indexOf(" ") + 1));
			}
		}

		if (condition.toUpperCase().contains("AND") || condition.toUpperCase().contains("OR")) {
			
			for (int i = (condition.indexOf("(") + 1); i < condition.length(); i = i + 2) {
				char c = condition.charAt(i);
				result.append(" " + column + notChar + c + " AND ");
			}
			result.replace(result.lastIndexOf(" A"), result.length(), "");
			result.insert(0, condition.substring(0, condition.indexOf(" ")));
			if (condition.contains(",")) {
				result.insert(result.indexOf(" "), " (");
				result.insert(result.length(), " ) ");
			}
			System.out.println("########################"+result);
			
		}
		return result;
	}
}
