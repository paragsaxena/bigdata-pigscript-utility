package bigdata;

import java.awt.List;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class DistinctStatement {
	public Map DistinctStatement(Map<String, Set<String>> filterAlias, FileOutputStream outputStream)
			throws IOException {
		Map<String, Set<String>> distinctAlias = new HashMap<String, Set<String>>();
		Map<String, Set<String>> distinctInfo = new HashMap<String, Set<String>>();
		distinctInfo = GenerateInfo.GenerateDistinctInfo();

		for (String entryset : distinctInfo.keySet()) {
			Set<String> tempSet = distinctInfo.get(entryset);
			Iterator it = tempSet.iterator();
			String tempString = "";
			while (it.hasNext()) {
				tempString = tempString + it.next() + ", ";
			}
			tempString = tempString.substring(0,tempString.length() - 2);
			String name = entryset.substring(entryset.indexOf("_"));
			System.out.println("generate" + name + " = FOREACH " + entryset + " GENERATE " + tempString + ";");
		}

		return null;
	}
}
