package bigdata;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.awt.*;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class LoadStatement 
{

	public Map LoadStatement(FileOutputStream outputStream) throws IOException 
	{
		Map<String, Set<String>> loadSource = new HashMap<String, Set<String>>();
		Map<String, Set<String>> loadSourceUpdated = new HashMap<String, Set<String>>();
		loadSource = GenerateInfo.GenerateLoadInfo();
		loadSourceUpdated = load(loadSource,outputStream);
		return loadSourceUpdated;
	}

	private Map load(Map<String, Set<String>> loadSource, FileOutputStream outputStream) 
	{
		Map<String, Set<String>> loadSourceUpdated = new HashMap<String, Set<String>>();
		for(String entryset : loadSource.keySet())
		{
			String[] loadTableName = entryset.split("\\.");
	        String rslt = "load_"+loadTableName[1]+" = LOAD $"+entryset+" USING org.apache.hive.hcatalog.pig.HCatLoader();"+'\n'+"";
			String temp = "load_" +loadTableName[1];
			loadSourceUpdated.put(temp, loadSource.get(entryset));
			byte[] bytesArray = rslt.getBytes();
	        //it.remove(); 
			try { outputStream.write(bytesArray); }
			catch (IOException e) {	e.printStackTrace(); }
		}
		/*while (it.hasNext()) 
		{
	        HashMap.Entry pair = (HashMap.Entry)it.next();
	        String temp = pair.getKey().toString();
	        String[] loadTableName = temp.split("\\.");
	        String rslt = "load_"+loadTableName[1]+" = LOAD $"+pair.getKey().toString()+" USING org.apache.hive.hcatalog.pig.HCatLoader();"+'\n'+"";
			
	        if (loadSource.containsKey(pair.getKey().toString()))
	        {
	        	System.out.println("loadKey: " + pair.getKey().toString() + " loadValues: " + pair.getValue().toString());
	        	loadSourceUpdated.put("load_"+loadTableName[1], loadSource.values());
	        }
	        byte[] bytesArray = rslt.getBytes();
	        it.remove(); 
			try { outputStream.write(bytesArray); }
			catch (IOException e) {	e.printStackTrace(); }
			//loadSource.put("load_"+loadTableName[1], loadSource.remove(loadTableName[1]));
		}*/
		return loadSourceUpdated;
	}
}
