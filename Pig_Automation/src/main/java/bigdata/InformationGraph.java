package bigdata;

import java.util.List;

public class InformationGraph 
{
	/**
	 * This function reads the data and the node to add
	 * 
	 * @return
	 * A new node created or updated node
	 */
	
	Node root;

	public Node Add(Node node, String data)
	{
		Node nodeToAdd = new Node(data);
		List<Node> listOfChildren = node.getChildren();
		if (!listOfChildren.contains(nodeToAdd))
		{
			node.getChildren().add(nodeToAdd);
			nodeToAdd.getParent().add(node);
		}
		if (node == root)
			return node;
		else
			return nodeToAdd;
	}
}
