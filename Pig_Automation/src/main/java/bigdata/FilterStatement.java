package bigdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FilterStatement 
{
	/*private StringBuilder filterStmt = new StringBuilder();

	public Map<String, Set<String>> FilterStatement(FileOutputStream outputStream) throws IOException 
	{
		Map<String, Set<String>> filterAlias = new HashMap<String, Set<String>>();
		Map<String, Map<String, String>> filterInfo = new HashMap<String, Map<String, String>>();
		filterInfo = GenerateInfo.GenerateFilterInfo();

		//Testing
		 for(String es : filterInfo.keySet()) 
		 { for (String e : filterInfo.get(es).keySet()) { 
				 System.out.println("Outer: "+es+" Inner: "+e+" Value: "+filterInfo.get(es).get(e).toString()); 
			} }
		 

		for (String keyset : filterInfo.keySet()) 
		{
			int numberColumns = filterInfo.get(keyset).size();
			String tableName = keyset.substring(keyset.indexOf("_"));
			if (numberColumns > 1)
				filterStmt.append("filter" + tableName + " = FILTER " + keyset + " BY " + "(");
			else
				filterStmt.append("filter" + tableName + " = FILTER " + keyset + " BY ");

			UpdateStatement(filterInfo.get(keyset));

			//System.out.println("@@"+filterStmt.toString());
			if (numberColumns>1)
			{
				filterStmt.replace(0, filterStmt.length(), filterStmt.substring(0, filterStmt.lastIndexOf("AND")));
			}
			else
			{
				filterStmt.replace(0, filterStmt.length(), filterStmt.substring(0, filterStmt.lastIndexOf(")")));
			}
			filterStmt.append(";"+'\n');
			
			filterAlias.put("filter"+tableName, filterInfo.get(keyset).keySet());
		}
		WriteStatement(outputStream);
		
		// Printing Filter Alias
		for (String key : filterAlias.keySet() )
			System.out.println("Key: "+ key + " Values: "+filterAlias.get(key));
		
		return filterAlias;
	}

	private void WriteStatement(OutputStream outputStream) 
	{
		String temp = "";
		//System.out.println("**"+filterStmt.toString());
		temp = filterStmt.toString();
		byte[] bytesArray = temp.getBytes();
		try {
			outputStream.write(bytesArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void UpdateStatement(Map<String, String> columnCondition) 
	{
		// StringBuilder result = new StringBuilder();
		for (String column : columnCondition.keySet()) 
		{
			String condition = columnCondition.get(column);

			String[] words = condition.split(" ");
			String temp = "";
			for (String word : words) {
				if (!word.equalsIgnoreCase("AND") && !word.equalsIgnoreCase("OR"))
					temp = temp + " " + word;
				else if (word.equalsIgnoreCase("AND")) 
				{
					filterStmt.append(UpdateCondition(column, temp));
					temp = "AND";
				} 
				else if (word.equalsIgnoreCase("OR")) 
				{
					filterStmt.append(UpdateCondition(column, temp));
					temp = "OR";
				}
			}
			filterStmt.append(UpdateCondition(column, temp));
			if (columnCondition.size()>1)
				filterStmt.append(") AND (");
			else
				filterStmt.append(") AND ");
		}
		
		// return result.toString();
	}

	private StringBuilder UpdateCondition(String column, String cond) 
	{
		String condition = cond.trim();
		//System.out.println("column: " + column + " Condition: " + condition);
		StringBuilder result = new StringBuilder();
		String notChar = "";
		if (condition.contains("!(")) 
			notChar = "!=";
		else if (condition.contains("(")) 
			notChar = "==";
		else 
		{
			if (condition.toUpperCase().contains("NULL") && (condition.toUpperCase().contains("AND") || condition.toUpperCase().contains("OR")))
			{
				result.append("" + condition.substring(0, condition.indexOf(" ")) +" "+column+ condition.substring(condition.indexOf(" "))+" ");
				//System.out.println(result.toString());
			}
			else if (condition.toUpperCase().contains("NULL"))
			{
				result.append(" " + column+" "+condition);
				//System.out.println(result.toString());
			}
			else 
			{
				if(condition.toUpperCase().contains("AND") || condition.toUpperCase().contains("OR"))
				{
					result.append(" " + condition.substring(0, condition.indexOf(" ")) + " " + column + " "+ condition.substring(condition.indexOf(" ") + 1));
					//System.out.println(result.toString());
				}
				else
				{
					result.append(" "+column + " "+ condition.substring(condition.indexOf(" ") + 1));
					//System.out.println(result.toString());
				}
			}
			return result;
		}

		if (condition.toUpperCase().contains("AND") || condition.toUpperCase().contains("OR")) 
		{		
			for (int i = (condition.indexOf("(") + 1); i < condition.length(); i = i + 2) {
				char c = condition.charAt(i);
				result.append(" " + column + notChar + c + " AND ");
			}
			result.replace(result.lastIndexOf(" A"), result.length(), "");
			result.insert(0, condition.substring(0, condition.indexOf(" ")));
			if (condition.contains(",")) {
				result.insert(result.indexOf(" "), " (");
				result.insert(result.length(), " ) ");
			}
			//System.out.println("########################"+result);			
		}
		else
		{
			for (int i = (condition.indexOf("(") + 1); i < condition.length(); i = i + 2) {
				char c = condition.charAt(i);
				result.append(" " + column + notChar + c + " AND ");
			}
			result.replace(result.lastIndexOf(" A"), result.length(), "");
			//result.insert(0, condition.substring(0, condition.indexOf(" ")));
			if (condition.contains(",")) {
				result.insert(result.indexOf(" "), " (");
				result.insert(result.length(), " ) ");
			}
			//System.out.println("########################"+result);			
		}
		return result;
	}*/
}
