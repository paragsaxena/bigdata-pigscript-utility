package bigdata;

import java.io.*;
import java.util.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateInfo 
{
	/**
	 * This function reads the STM sheet and develops a 
	 * DAG of all the transformation
	 * 
	 * @return
	 * 	A DAG of columns with the transformations as nodes
	 * 
	 * @throws IOException
	 */
	
	public static InformationGraph GenerateInfoGraph() throws IOException
	{
		FileInputStream inputStream = new FileInputStream(new File(PigUtility.excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet s = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = s.iterator();
		
		InformationGraph info = new InformationGraph();
		Node node = new Node();
		
		while (rowIterator.hasNext()) 
		{
			Row nextRow = rowIterator.next();
			if (nextRow.getRowNum()!=0 && nextRow.getRowNum()!=1 && nextRow.getCell(0) != null)
			{
				String sourceTableName = nextRow.getCell(1).toString();
				String sourceColumnName = nextRow.getCell(2).toString();
				String transformation = nextRow.getCell(4).toString();
				String targetTableName = nextRow.getCell(5).toString();
				String targetColumnName = nextRow.getCell(6).toString();
				String[] linesOfTransformation = transformation.split("\\r?\\n");
				
				// Adding Root Node
				if (info.root == null)
				{
					info.root = new Node("LOAD TABLE "+sourceTableName);
					node = info.root;
				}
				
				// Adding Source Column as the first Node
				node = info.Add(node,sourceColumnName);
				
				// Searching for the Source Column Node
				List<Node> listOfChildrenOfRoot = node.getChildren();
				for (Node childOfRoot : listOfChildrenOfRoot)
					if (childOfRoot.getData().equals(sourceColumnName))
						node = childOfRoot;
				
				// Adding Transformation Nodes for the respective Source Column
				for (String line : linesOfTransformation)
					node = info.Add(node, line);
				
				// Adding Target information as the Leaf Node
				String targetInfo = "TARGET TABLE: "+targetTableName+" TARGET COLUMN: "+targetColumnName;
				info.Add(node, targetInfo);
				
				node = info.root;
			}
		}
		return info;
	}	
}
