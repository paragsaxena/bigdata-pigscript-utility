package bigdata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class GenerateStatement 
{
	/**
	 * This function reads the graph and the output stream
	 * 
	 * @return
	 * A pig script from the graph intake
	 */
	
	Set<String> loadSourceTables = new HashSet<String>();
	public GenerateStatement(InformationGraph graph, OutputStream outputStream)
	{
		if (graph.root == null)
		{
			System.out.println("Graph is Empty or does not have ant root");
			return;
		}
		else if (graph.root.getData().toUpperCase().contains("LOAD"))
		{
			System.out.println("Writing LOAD Statements to the Pig File");
			WriteLoadStatement(graph, outputStream);
		}
		
		List<Node> listOfWorkingNodes = new LinkedList<Node>();
		List<Node> listOfRootChildren = graph.root.getChildren();
		
		for (Node rootChild : listOfRootChildren)
		{
			List<Node> listOfChildren = rootChild.getChildren();
			for (Node child : listOfChildren) 
			{
				if (listOfWorkingNodes.contains(child.getParent()))
					continue;
				else
				{
					listOfWorkingNodes = new LinkedList<Node>();
					listOfWorkingNodes.addAll(child.getParent());
					WriteStatement(listOfWorkingNodes);
				}
			}
		}	
	}
	
	private void WriteStatement(List<Node> listOfWorkingNodes) 
	{
		for (Node workingNode : listOfWorkingNodes)
		{
			if (workingNode.getData().toUpperCase().contains("JOIN"))
			{
				
			}
			else if (workingNode.getData().toUpperCase().contains("GROUP"))
			{
				
			}
			else if (workingNode.getData().toUpperCase().contains("GENERATE"))
			{
				
			}
		}
	}

	public void WriteLoadStatement(InformationGraph graph, OutputStream outputStream)
	{
		TraverseAndFindNode(graph.root);
		
		for (String table : loadSourceTables)
		{
			if (table.contains("."))
			{
				String sourceTableName = table.substring(table.indexOf(".")+1);
				String rslt = "load_"+sourceTableName+" = LOAD \'"+table+"\' USING org.apache.hive.hcatalog.pig.HCatLoader();"+'\n';		
				
				byte[] bytesArray = rslt.getBytes();
				try { outputStream.write(bytesArray); }
				catch (IOException e) {	e.printStackTrace(); }
			}
			else
				System.out.println("Invalid Source Table Name: "+table);
		}
	}

	private void TraverseAndFindNode(Node node) 
	{
		if (!node.getChildren().isEmpty())
			for (Node child : node.getChildren())
				TraverseAndFindNode(child);

		String dataOfCurrentNode = node.getData();
		String tableInformation = "";
		if (!dataOfCurrentNode.isEmpty())
		{
			if (dataOfCurrentNode.toUpperCase().contains("LOAD"))
				tableInformation = dataOfCurrentNode.substring(dataOfCurrentNode.toUpperCase().indexOf("DB_")).trim();
			if (dataOfCurrentNode.toUpperCase().contains("JOIN"))
				tableInformation = dataOfCurrentNode.substring(dataOfCurrentNode.toUpperCase().indexOf("DB_"),dataOfCurrentNode.toUpperCase().indexOf("ON")).trim();
			if (!tableInformation.isEmpty())
				loadSourceTables.add(tableInformation);
		}
	}
}
