package bigdata;

import java.awt.List;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class DistinctStatement 
{
	private StringBuilder distinctStmt = new StringBuilder();
	private StringBuilder generateStmt = new StringBuilder();
	
	/*public Map DistinctStatement(FileOutputStream outputStream) throws IOException 
	{
		Map<String, Set<String>> distinctAlias = new HashMap<String, Set<String>>();
		Map<String, Set<String>> distinctInfo = new HashMap<String, Set<String>>();
		distinctInfo = GenerateInfo.GenerateDistinctInfo();

		//Testing
		for(String es : distinctInfo.keySet()) 
		{  System.out.println("Key: "+es+" Value: "+distinctInfo.get(es).toString());  }
		
		String typeOfStatement = "";
		
		Set<String> forEachAlias = new HashSet<String>();
		forEachAlias = GenerateForEachStatement(distinctInfo);
		typeOfStatement = "ForEach";
		WriteStatement(outputStream,typeOfStatement);
		
		GenerateDistinctStatement(forEachAlias);
		typeOfStatement = "Distinct";
		WriteStatement(outputStream,typeOfStatement);
		
		 return null;
	}

	private void WriteStatement(FileOutputStream outputStream, String type) 
	{
		String temp = "";
		if (type.toUpperCase().equals("FOREACH"))	
			temp = generateStmt.toString();
		else
			temp = distinctStmt.toString();
		byte[] bytesArray = temp.getBytes();
		try {
			outputStream.write(bytesArray);
		} catch (IOException e) {
			e.printStackTrace(); }
	}
		

	private void GenerateDistinctStatement(Set<String> forEachAlias) 
	{
		Iterator<String> it = forEachAlias.iterator();
		while (it.hasNext())
		{
			String alias = it.next();
			distinctStmt.append("distinct"+alias.substring(alias.indexOf("_"))+" = DISTINCT "+alias+";"+'\n');
		}
	}

	private Set GenerateForEachStatement(Map<String, Set<String>> distinctInfo) 
	{
		Set<String> forEachAlias = new HashSet<String>();
		for (String keyset : distinctInfo.keySet()) 
		{
			StringBuffer columns = new StringBuffer();
			for (String columnSet : distinctInfo.get(keyset))
			{
				columns.append(columnSet+", ");
			}
			
			generateStmt.append("generate"+keyset.substring(keyset.indexOf("_"))+" = FOREACH "+keyset+" GENERATE "+columns.substring(0,columns.lastIndexOf(","))+" ;"+'\n');
			forEachAlias.add("generate"+keyset.substring(keyset.indexOf("_")));
		}
		return forEachAlias;
		
	}*/
}
