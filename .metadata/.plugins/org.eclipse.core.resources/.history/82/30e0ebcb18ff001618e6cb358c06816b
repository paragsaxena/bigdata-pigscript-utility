package bigdata;

import java.io.*;
import java.util.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateInfo 
{
	/**
	 * This function reads the STM sheet and develops a 
	 * DAG of all the transformation
	 * 
	 * @return
	 * 	A DAG of columns with the transformations as nodes
	 * 
	 * @throws IOException
	 */
	
	public static InformationGraph GenerateInfoGraph() throws IOException
	{
		FileInputStream inputStream = new FileInputStream(new File(PigUtility.excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet s = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = s.iterator();
		
		InformationGraph info = new InformationGraph();
		Node node = new Node();
		
		while (rowIterator.hasNext()) 
		{
			Row nextRow = rowIterator.next();
			if (nextRow.getRowNum()!=0 && nextRow.getRowNum()!=1 && nextRow.getCell(0) != null)
			{
				String sourceTableName = nextRow.getCell(1).toString();
				String sourceColumnName = nextRow.getCell(2).toString();
				
				if (info.root == null)
				{
					info.root = new Node("LOAD TABLE "+sourceTableName);
					node = info.root;
				}
				node = info.AddFirst(node,sourceColumnName);
			}
		}
				
		rowIterator = s.iterator();
		while (rowIterator.hasNext()) 
		{
			Row nextRow = rowIterator.next();
			if (nextRow.getRowNum()!=0 && nextRow.getRowNum()!=1 && nextRow.getCell(0) != null)
			{
				String sourceColumnName = nextRow.getCell(2).toString();
				String transformation = nextRow.getCell(4).toString();
				String targetTableName = nextRow.getCell(5).toString();
				String targetColumnName = nextRow.getCell(6).toString();
				String[] lines = transformation.split("\\r?\\n");
				
				List<Node> listOfChild = info.root.children;
				for (Node child : listOfChild)
					if (child.data.equals(sourceColumnName))
						node = child;
				
				for (String line : lines)
					node = info.Add(node, line);
				
				String targetInfo = "TARGET TABLE: "+targetTableName+" TARGET COLUMN: "+targetColumnName;
				info.Add(node, targetInfo);
				node = info.root;
			}
		}
		return info;
	}	
}
