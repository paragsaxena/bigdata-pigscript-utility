package bigdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateInfo 
{
	private static Set<String> sourceColumnS = new HashSet<String>();
	private static Map<String, Set<String>> tableColumnHM = new HashMap<String, Set<String>>();
	
	public static Map GenerateLoadInfo() throws IOException
	{
		FileInputStream inputStream = new FileInputStream(new File(PigUtility.excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet s = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = s.iterator();

		while (rowIterator.hasNext()) 
		{
			Row nextRow = rowIterator.next();
			if (nextRow.getRowNum()!=0 && nextRow.getCell(0) != null)
			{
				if(tableColumnHM.containsKey(nextRow.getCell(0).toString()))
				{
					continue;
				}
				else
				{
					Set newSet = new HashSet<>();
					newSet.add(nextRow.getCell(1).toString());
					tableColumnHM.put(nextRow.getCell(0).toString(), newSet);
				}
				
				String joinTable = nextRow.getCell(5).toString();
				if (!joinTable.equalsIgnoreCase("NA"))
				{
					joinInformation(joinTable);
				}
			}
		}	
		return tableColumnHM;
	}
	
	private static void joinInformation(String s) 
	{
		String[] str = s.split(" ");
		for (int i=0; i<str.length; i++)
		{
			if (str[i].startsWith("db_"))
			{
				if(!tableColumnHM.containsKey(str[i]))
				{
					Set newSet = new HashSet<>();
					for (int j=4; j<str.length; j++)
					{
						newSet.add(str[j]);
					}
					tableColumnHM.put(str[i], newSet);
				}
			}
		}
	}
	
	public static Map GenerateFilterInfo() throws IOException
	{
		FileInputStream inputStream = new FileInputStream(new File(PigUtility.excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet s = workbook.getSheetAt(0);
		
		Map<String,Map<String,String>> filterInfo = new HashMap<String,Map<String,String>>();
		Map<String, String> columnMap = new HashMap<String, String>();

		Iterator<Row> rowIterator = s.iterator();
		while (rowIterator.hasNext()) 
		{
			Row nextRow = rowIterator.next();
			if (nextRow.getRowNum()!=0 && nextRow.getCell(0) != null)
			{
				if (!nextRow.getCell(2).toString().equalsIgnoreCase("NA"))
				{
					String[] temp = nextRow.getCell(0).toString().split("\\.");
					String key = "load_"+temp[1];
					String column = nextRow.getCell(1).toString();
					String condition = nextRow.getCell(2).toString();
					columnMap.put(column, condition);
					filterInfo.put(key, columnMap);
				}
			}
		}
		return filterInfo;
	}

	public static Map GenerateDistinctInfo() throws IOException
	{
		FileInputStream inputStream = new FileInputStream(new File(PigUtility.excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet s = workbook.getSheetAt(0);
		
		Map<String,Set<String>> distinctInfo = new HashMap<String,Set<String>>();
		
		Iterator<Row> rowIterator = s.iterator();
		while (rowIterator.hasNext()) 
		{
			Row nextRow = rowIterator.next();
			if (nextRow.getRowNum()!=0 && nextRow.getCell(0) != null)
			{
				if (nextRow.getCell(3).toString().equalsIgnoreCase("Yes"))
				{
					if(distinctInfo.containsKey(nextRow.getCell(0).toString()))
					{
						continue;
					}
					else
					{
						Set newSet = new HashSet<>();
						newSet.add(nextRow.getCell(1).toString());
						String[] temp = nextRow.getCell(0).toString().split("\\.");
						String key = "filter_"+temp[1];
						distinctInfo.put(key, newSet);
					}
				}
			}
		}
		return distinctInfo;
	}
}
