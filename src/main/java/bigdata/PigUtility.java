package bigdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PigUtility {
	public static String excelFilePath = "C:/Users/parag.saxena/Desktop/STM_To_Pig_Script/STM_Template1.xlsx";
	public static String outputPath = "C:/Users/parag.saxena/Desktop/STM_To_Pig_Script/STM_Pig.pig";
	public static void main(String[] args) throws IOException 
	{
		
		FileOutputStream outputStream = new FileOutputStream(outputPath);
		
		LoadStatement ls = new LoadStatement();
		Map<String, Set<String>> loadSourceAlias = new HashMap<String, Set<String>>();
		loadSourceAlias = ls.LoadStatement(outputStream);
		
		Iterator it = loadSourceAlias.entrySet().iterator();
				
		FilterStatement fs = new FilterStatement();
		Map<String, Set<String>> filterAlias = new HashMap<String, Set<String>>();
		filterAlias = fs.FilterStatement(loadSourceAlias,outputStream);
		
		/*DistinctStatement ds = new DistinctStatement();
		Map<String, Set<String>> distinctAlias = new HashMap<String, Set<String>>();
		distinctAlias = ds.DistinctStatement(filterAlias,outputStream);*/
		
		/*GroupByStatement gbs = new GroupByStatement(firstSheet,outputStream);
		JoinStatement js = new JoinStatement(firstSheet,outputStream);
		OrderByStatement obs = new OrderByStatement(firstSheet,outputStream);*/
		
		System.out.println("Success in Main");
		outputStream.close();
	}
}
